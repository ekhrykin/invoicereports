package com.gohnxp.ekhrykin;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.function.Consumer;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.gmail.gohnxp.BilldocsApplication;
import com.gmail.gohnxp.domain.Company;
import com.gmail.gohnxp.repository.CompanyRepository;
import com.google.common.collect.Iterables;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BilldocsApplication.class)
@WebAppConfiguration
public class CompanyCreateTests {

	@Autowired
	private WebApplicationContext context;
	private MockMvc mvc;
	
	private static String TEST_NAME = "testCompany";
	private static String TEST_ADDR = "test str.";
	private static String TEST_INN = "testINN";
	private static String TEST_KPP = "testKPP";
	
	@Autowired
	private CompanyRepository compRep;
	
	@Before
	public void setUp() {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}

	@Test
	public void createCompany() throws Exception {
		
		this.mvc.perform(
				post("/api/companies").content("{\"name\": \"" + TEST_NAME 
						+ "\", \"address\": \"" + TEST_ADDR + "\"," 
						+ "\"inn\": \"" + TEST_INN + "\"," 
						+ "\"kpp\": \"" + TEST_KPP + "\"}"))
				.andExpect(status().isCreated());
		
		Iterable<Company> cs = compRep.findAllByNameContaining(TEST_NAME);
		
		Assert.assertTrue("Company not created!", !Iterables.isEmpty(cs));
		
		cs.forEach(new Consumer<Company>(){
			@Override
			public void accept(Company c) {
				compRep.delete(c);
			}			
		});
	}

}
