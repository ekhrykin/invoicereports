package com.gohnxp.ekhrykin;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.gmail.gohnxp.BilldocsApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BilldocsApplication.class)
@WebAppConfiguration
public class ReportsSearchTests {

	@Autowired
	private WebApplicationContext context;
	private MockMvc mvc;
	
	private static Integer TEST_INVOICE_INFO_ID = 1;
	private static Integer TEST_REPORT_TYPE = 1;

	@Value("${reports.first_report.dest_file_name_path_with_prefix}")
	private String firstReportDestPathPrefix;

	@Value("${reports.second_report.dest_file_name_path_with_prefix}")
	private String secondReportDestPathPrefix;

	@Value("${reports.third_report.dest_file_name_path_with_prefix}")
	private String thirdReportDestPathPrefix;
		
	@Before
	public void setUp() throws IOException {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}

	@Test
	public void test1() throws Exception {
		Assert.assertTrue("Reports not created!", 
				Files.exists(Paths.get(firstReportDestPathPrefix + TEST_INVOICE_INFO_ID + ".pdf"))
				&& Files.exists(Paths.get(secondReportDestPathPrefix + TEST_INVOICE_INFO_ID + ".pdf"))
				&& Files.exists(Paths.get(thirdReportDestPathPrefix + TEST_INVOICE_INFO_ID + ".pdf")));
	}

	@Test
	public void test2() throws Exception {
		
		this.mvc.perform(
				get("/api/invoice_info/" + TEST_INVOICE_INFO_ID + "/reports/" + TEST_REPORT_TYPE))
				.andExpect(status().isOk());
	}
}
