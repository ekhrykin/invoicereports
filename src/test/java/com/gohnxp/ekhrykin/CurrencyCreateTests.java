package com.gohnxp.ekhrykin;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.function.Consumer;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.gmail.gohnxp.BilldocsApplication;
import com.gmail.gohnxp.domain.Currency;
import com.gmail.gohnxp.repository.CurrencyRepository;
import com.google.common.collect.Iterables;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BilldocsApplication.class)
@WebAppConfiguration
public class CurrencyCreateTests {

	@Autowired
	private WebApplicationContext context;
	private MockMvc mvc;
	
	private static String TEST_NAME = "testCurr";
	private static String TEST_NUMBER = "111";
	
	@Autowired
	private CurrencyRepository currRep;
	
	@Before
	public void setUp() {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}

	@Test
	public void createCA() throws Exception {
		
		this.mvc.perform(
				post("/api/currencies").content("{\"number\": \"" + TEST_NUMBER 
						+ "\", \"name\": \"" + TEST_NAME + "\"}"))
				.andExpect(status().isCreated());
		
		Iterable<Currency> currs = currRep.findAllByName(TEST_NAME);
		
		Assert.assertTrue("Currency not created!", !Iterables.isEmpty(currs));
		
		currs.forEach(new Consumer<Currency>(){
			@Override
			public void accept(Currency curr) {
				currRep.delete(curr);
			}			
		});
	}

}
