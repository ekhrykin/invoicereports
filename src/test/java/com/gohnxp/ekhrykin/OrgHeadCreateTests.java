package com.gohnxp.ekhrykin;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.function.Consumer;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.gmail.gohnxp.BilldocsApplication;
import com.gmail.gohnxp.domain.OrgHead;
import com.gmail.gohnxp.repository.OrgHeadRepository;
import com.google.common.collect.Iterables;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BilldocsApplication.class)
@WebAppConfiguration
public class OrgHeadCreateTests {

	@Autowired
	private WebApplicationContext context;
	private MockMvc mvc;
	
	private static String TEST_FIO = "testOH";
	private static String TEST_START_DATE = "1900-00-00";
	
	@Autowired
	private OrgHeadRepository ohRep;
	
	@Before
	public void setUp() {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}

	@Test
	public void createCA() throws Exception {
		
		this.mvc.perform(
				post("/api/org_heads").content("{\"fio\": \"" + TEST_FIO 
						+ "\", \"startDate\": \"" + TEST_START_DATE + "\"}"))
				.andExpect(status().isCreated());
		
		Iterable<OrgHead> ohs = ohRep.findAllByFio(TEST_FIO);
		
		Assert.assertTrue("OH not created!", !Iterables.isEmpty(ohs));
		
		ohs.forEach(new Consumer<OrgHead>(){
			@Override
			public void accept(OrgHead ca) {
				ohRep.delete(ca);
			}			
		});
	}

}
