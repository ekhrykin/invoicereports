package com.gohnxp.ekhrykin;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.function.Consumer;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.gmail.gohnxp.BilldocsApplication;
import com.gmail.gohnxp.domain.TaxRate;
import com.gmail.gohnxp.repository.TaxRateRepository;
import com.google.common.collect.Iterables;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BilldocsApplication.class)
@WebAppConfiguration
public class TaxRateCreateTests {

	@Autowired
	private WebApplicationContext context;
	private MockMvc mvc;
	
	private static Integer TEST_VALUE = 999;
	private static String TEST_START_DATE = "1900-00-00";
	
	@Autowired
	private TaxRateRepository trRep;
	
	@Before
	public void setUp() {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}

	@Test
	public void createTaxRate() throws Exception {
		
		this.mvc.perform(
				post("/api/tax_rates").content("{\"value\": \"" + TEST_VALUE 
						+ "\", \"startDate\": \"" + TEST_START_DATE + "\"}"))
				.andExpect(status().isCreated());
		
		Iterable<TaxRate> trs = trRep.findAllByValue(TEST_VALUE);
		
		Assert.assertTrue("OH not created!", !Iterables.isEmpty(trs));
		
		trs.forEach(new Consumer<TaxRate>(){
			@Override
			public void accept(TaxRate tr) {
				trRep.delete(tr);
			}			
		});
	}

}
