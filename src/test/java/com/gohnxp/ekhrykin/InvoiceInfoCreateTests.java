package com.gohnxp.ekhrykin;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.gmail.gohnxp.BilldocsApplication;
import com.gmail.gohnxp.domain.InvoiceData;
import com.gmail.gohnxp.domain.InvoiceInfo;
import com.gmail.gohnxp.repository.InvoiceDataRepository;
import com.gmail.gohnxp.repository.InvoiceInfoRepository;
import com.google.common.collect.Iterables;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BilldocsApplication.class)
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InvoiceInfoCreateTests {

	@Autowired
	private WebApplicationContext context;
	private MockMvc mvc;
	
	private static String TEST_NUMBER = "TEST_II_NUMBER";
	private static String TEST_INVOICE_DATE = "1900-00-00";
	private static Integer TEST_SELLER_ID = 1;
	private static Integer TEST_CONTR_ID = 1;
	private static Integer TEST_CA_ID = 1;
	private static Integer TEST_OH_ID = 1;
	private static Integer TEST_TAX_RATE_ID = 1;
	private static Integer TEST_CURR_ID = 1;
		
	private static Integer TEST_II_ID;
	private static String TEST_PROD_NAME = "testProd";
	private static Integer TEST_MEASURE_ID = 1;
	private static Integer TEST_COUNT = 1;
	private static Integer TEST_PROD_PRICE = 1;
	
	@Autowired
	private InvoiceInfoRepository iiRep;
	@Autowired
	private InvoiceDataRepository idRep;
	
	@Before
	public void setUp() {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}

	@Test
	public void test1() throws Exception {
		
		this.mvc.perform(
				post("/api/invoice_infos").content("{\"invoiceNumber\": \"" + TEST_NUMBER 
						+ "\", \"invoiceDate\": \"" + TEST_INVOICE_DATE  
						+ "\", \"seller\": \"/api/companies/" + TEST_SELLER_ID 
						+ "\", \"contractor\": \"/api/companies/" + TEST_CONTR_ID  
						+ "\", \"chiefAccountant\": \"/api/chief_accountants/" + TEST_CA_ID  
						+ "\", \"orgHead\": \"/api/org_heads/" + TEST_OH_ID  
						+ "\", \"taxRate\": \"/api/tax_rates/" + TEST_TAX_RATE_ID
						+ "\", \"currency\": \"/api/currencies/" + TEST_CURR_ID + "\"}"))
				.andExpect(status().isCreated());
		
		Iterable<InvoiceInfo> iis = iiRep.findAllByInvoiceNumber(TEST_NUMBER);
		
		Assert.assertTrue("InvoiceInfo not created!", !Iterables.isEmpty(iis));
		
		TEST_II_ID = iis.iterator().next().getId();
	}

	@Test
	public void test2() throws Exception {
		
		this.mvc.perform(
				post("/api/invoice_datas").content(
						"{\"invoiceInfo\": \"/api/invoice_infos/" + TEST_II_ID 
						+ "\", \"productName\": \"" + TEST_PROD_NAME  
						+ "\", \"measure\": \"/api/measures/" + TEST_MEASURE_ID 
						+ "\", \"count\": \"" + TEST_COUNT  
						+ "\", \"productPrice\": \"" + TEST_PROD_PRICE  
						+ "\"}"))
				.andExpect(status().isCreated());
		
		Iterable<InvoiceData> ids = idRep.findAllByInvoiceInfoId(TEST_II_ID);
		
		Assert.assertTrue("InvoiceData not created!", !Iterables.isEmpty(ids));
	}

	@Test
	@Ignore
	public void test3() throws Exception {
		
		this.mvc.perform(
				get("/api/invoice_datas/search/deleteProductByInvoiceInfoId?invoiceInfoId=" + TEST_II_ID))
				.andExpect(status().is2xxSuccessful());
		
		Iterable<InvoiceData> ids = idRep.findAllByInvoiceInfoId(TEST_II_ID);		
		Assert.assertTrue("InvoiceData not deleted!", Iterables.isEmpty(ids));
	}

	@Test
	public void test4() throws Exception {
		
		this.mvc.perform(
				get("/api/invoice_infos/search/deleteBillByInvoiceNumber?invoiceNumber=" + TEST_NUMBER))
				.andExpect(status().is2xxSuccessful());

		Iterable<InvoiceInfo> iis = iiRep.findAllByInvoiceNumber(TEST_NUMBER);
		Assert.assertTrue("InvoiceInfo not deleted!", Iterables.isEmpty(iis));
		
		Iterable<InvoiceData> ids = idRep.findAllByInvoiceInfoId(TEST_II_ID);		
		Assert.assertTrue("InvoiceData not deleted!", Iterables.isEmpty(ids));
	}

	@Test
	@Ignore
	public void test5() throws Exception {
		
		this.mvc.perform(
				delete("/api/invoice_infos/" + TEST_II_ID))
				.andExpect(status().is2xxSuccessful());

		Iterable<InvoiceInfo> iis = iiRep.findAllByInvoiceNumber(TEST_NUMBER);
		Assert.assertTrue("InvoiceInfo not deleted!", Iterables.isEmpty(iis));
		
		Iterable<InvoiceData> ids = idRep.findAllByInvoiceInfoId(TEST_II_ID);		
		Assert.assertTrue("InvoiceData not deleted!", Iterables.isEmpty(ids));
	}

}
