package com.gohnxp.ekhrykin;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.function.Consumer;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.gmail.gohnxp.BilldocsApplication;
import com.gmail.gohnxp.domain.Measure;
import com.gmail.gohnxp.repository.MeasureRepository;
import com.google.common.collect.Iterables;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BilldocsApplication.class)
@WebAppConfiguration
public class MeasureCreateTests {

	@Autowired
	private WebApplicationContext context;
	private MockMvc mvc;
	
	private static String TEST_NAME = "testMeasure";
	private static String TEST_NUMBER = "111";
	
	@Autowired
	private MeasureRepository measureRep;
	
	@Before
	public void setUp() {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}

	@Test
	public void createCA() throws Exception {
		
		this.mvc.perform(
				post("/api/measures").content("{\"number\": \"" + TEST_NUMBER 
						+ "\", \"name\": \"" + TEST_NAME + "\"}"))
				.andExpect(status().isCreated());
		
		Iterable<Measure> measures = measureRep.findAllByName(TEST_NAME);
		
		Assert.assertTrue("Currency not created!", !Iterables.isEmpty(measures));
		
		measures.forEach(new Consumer<Measure>(){
			@Override
			public void accept(Measure m) {
				measureRep.delete(m);
			}			
		});
	}

}
