package com.gohnxp.ekhrykin;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.gmail.gohnxp.BilldocsApplication;
import com.gmail.gohnxp.domain.InvoiceInfo;
import com.gmail.gohnxp.repository.InvoiceInfoRepository;
import com.google.common.collect.Iterables;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BilldocsApplication.class)
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InvoiceInfoFindTests {

	@Autowired
	private WebApplicationContext context;
	private MockMvc mvc;
	
	@Autowired
	private InvoiceInfoRepository iiRep;
	
	private static String DATE_START = "2014-11-02";
	private static String DATE_END_SMALL = "2014-11-03";
	private static String DATE_END_BIG = "2018-11-03";
	private DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	private static Integer CONTRACTOR_ID = 2;
	
	@Before
	public void setUp() throws ParseException {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}

	@Test
	public void test1() throws Exception {				
		Iterable<InvoiceInfo> iis = iiRep.findAllByInvoiceDateBetweenAndContractorId(
				df.parse(DATE_START), df.parse(DATE_END_SMALL), CONTRACTOR_ID);		
		Assert.assertTrue("Find method has bug!", Iterables.isEmpty(iis));
	}

	@Test
	public void test2() throws Exception {				
		Iterable<InvoiceInfo> iis = iiRep.findAllByInvoiceDateBetweenAndContractorId(
				df.parse(DATE_START), df.parse(DATE_END_BIG), CONTRACTOR_ID);		
		Assert.assertTrue("Find method has bug!", !Iterables.isEmpty(iis));
	}
	
	@Test
	public void test3() throws Exception{

		this.mvc.perform(
				get("/api/invoice_infos/search/findAllByInvoiceDateBetweenAndContractorId?caseDateStart="
						+ DATE_START
						+ "&caseDateEnd="
						+ DATE_END_BIG
						+ "&contractorId=" + CONTRACTOR_ID)).andExpect(status().isOk());
	}

}
