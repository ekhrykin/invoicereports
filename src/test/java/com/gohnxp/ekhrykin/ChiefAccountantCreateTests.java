package com.gohnxp.ekhrykin;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.function.Consumer;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.gmail.gohnxp.BilldocsApplication;
import com.gmail.gohnxp.domain.ChiefAccountant;
import com.gmail.gohnxp.repository.ChiefAccountantRepository;
import com.google.common.collect.Iterables;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BilldocsApplication.class)
@WebAppConfiguration
public class ChiefAccountantCreateTests {

	@Autowired
	private WebApplicationContext context;
	private MockMvc mvc;
	
	private static String TEST_FIO = "testCA";
	private static String TEST_START_DATE = "1900-00-00";
	
	@Autowired
	private ChiefAccountantRepository caRep;
	
	@Before
	public void setUp() {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}

	@Test
	public void createCA() throws Exception {
		
		this.mvc.perform(
				post("/api/chief_accountants").content("{\"fio\": \"" + TEST_FIO 
						+ "\", \"startDate\": \"" + TEST_START_DATE + "\"}"))
				.andExpect(status().isCreated());
		
		Iterable<ChiefAccountant> cas = caRep.findAllByFio(TEST_FIO);
		
		Assert.assertTrue("CA not created!", !Iterables.isEmpty(cas));
		
		cas.forEach(new Consumer<ChiefAccountant>(){
			@Override
			public void accept(ChiefAccountant ca) {
				caRep.delete(ca);
			}			
		});
	}

}
