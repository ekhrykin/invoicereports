package com.gmail.gohnxp.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.gmail.gohnxp.domain.Company;

@RepositoryRestResource(collectionResourceRel = "companies", path = "companies")
public interface CompanyRepository extends PagingAndSortingRepository<Company, Integer> {

	Iterable<Company> findAllByNameContaining(@Param("name") String name);
	Company findByName(@Param("name") String name);
}
