package com.gmail.gohnxp.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.transaction.annotation.Transactional;

import com.gmail.gohnxp.domain.InvoiceInfo;

@RepositoryRestResource(collectionResourceRel = "invoice_infos", path = "invoice_infos")
@Transactional
public interface InvoiceInfoRepository extends PagingAndSortingRepository<InvoiceInfo, Integer> {

	Iterable<InvoiceInfo> findAllByInvoiceNumber(@Param("invoiceNumber") String invoiceNumber);
	Iterable<InvoiceInfo> findAllByInvoiceDateBetweenAndContractorId(
			@Param("caseDateStart") @DateTimeFormat(iso = ISO.DATE) Date caseDateStart,
			@Param("caseDateEnd") @DateTimeFormat(iso = ISO.DATE) Date caseDateEnd,
			@Param("contractorId") Integer contractorId);

    @Modifying
    @Transactional
    @Query("delete from InvoiceInfo i where i.invoiceNumber = ?1")
	Integer deleteBillByInvoiceNumber(@Param("invoiceNumber") String invoiceNumber);
}
