package com.gmail.gohnxp.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.gmail.gohnxp.domain.Measure;

@RepositoryRestResource(collectionResourceRel = "measures", path = "measures")
public interface MeasureRepository extends PagingAndSortingRepository<Measure, Integer> {

	Iterable<Measure> findAllByName(@Param("name") String name);
}
