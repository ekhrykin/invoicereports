package com.gmail.gohnxp.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

import com.gmail.gohnxp.domain.InvoiceData;

@RepositoryRestResource(collectionResourceRel = "invoice_datas", path = "invoice_datas")
@Transactional
public interface InvoiceDataRepository extends PagingAndSortingRepository<InvoiceData, Integer> {

	Iterable<InvoiceData> findAllByInvoiceInfoId(@Param("invoiceInfoId") Integer invoiceInfoId);

    @Modifying
    @Transactional
    @Query("delete from InvoiceData d where d.invoiceInfo.id = ?1")
	Integer deleteProductByInvoiceInfoId(@Param("invoiceInfoId") Integer invoiceInfoId);
}
