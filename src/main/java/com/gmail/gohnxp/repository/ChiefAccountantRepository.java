package com.gmail.gohnxp.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.gmail.gohnxp.domain.ChiefAccountant;

@RepositoryRestResource(collectionResourceRel = "chief_accountants", path = "chief_accountants")
public interface ChiefAccountantRepository extends PagingAndSortingRepository<ChiefAccountant, Integer> {

	Iterable<ChiefAccountant> findAllByFio(@Param("fio") String fio);
	ChiefAccountant findByFio(@Param("fio") String fio);
}
