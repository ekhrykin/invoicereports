package com.gmail.gohnxp.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.gmail.gohnxp.domain.Currency;

@RepositoryRestResource(collectionResourceRel = "currencies", path = "currencies")
public interface CurrencyRepository extends PagingAndSortingRepository<Currency, Integer> {

	Iterable<Currency> findAllByName(@Param("name") String name);
	Currency findByName(@Param("name") String name);
}
