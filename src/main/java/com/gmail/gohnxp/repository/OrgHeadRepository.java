package com.gmail.gohnxp.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.gmail.gohnxp.domain.OrgHead;

@RepositoryRestResource(collectionResourceRel = "org_heads", path = "org_heads")
public interface OrgHeadRepository extends PagingAndSortingRepository<OrgHead, Integer> {

	Iterable<OrgHead> findAllByFio(@Param("fio") String fio);
	OrgHead findByFio(@Param("fio") String fio);
}
