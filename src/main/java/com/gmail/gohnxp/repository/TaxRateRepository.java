package com.gmail.gohnxp.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.gmail.gohnxp.domain.TaxRate;

@RepositoryRestResource(collectionResourceRel = "tax_rates", path = "tax_rates")
public interface TaxRateRepository extends PagingAndSortingRepository<TaxRate, Integer> {

	Iterable<TaxRate> findAllByValue(@Param("value") Integer value);
	TaxRate findByValue(@Param("value") Integer value);
}
