package com.gmail.gohnxp.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Company implements Serializable  {
	
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="comp_gen", sequenceName="comp_seq", allocationSize=1, initialValue=2)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="comp_gen")
	private Integer id;

	@Column(nullable = false)
	private String name;

	@Column()
	private String address;

	@Column(nullable = false)
	private String inn;

	@Column(nullable = false)
	private String kpp;

	public Company() {
		super();
	}

	public Company(Integer id, String name, String address, String inn, String kpp) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.inn = inn;
		this.kpp = kpp;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getInn() {
		return inn;
	}

	public void setInn(String inn) {
		this.inn = inn;
	}

	public String getKpp() {
		return kpp;
	}

	public void setKpp(String kpp) {
		this.kpp = kpp;
	}	
}
