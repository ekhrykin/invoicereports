package com.gmail.gohnxp.domain;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class InvoiceInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "ii_gen", sequenceName = "ii_seq", allocationSize = 1, initialValue = 2)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ii_gen")
	private Integer id;

	@Column(name = "invoice_number", nullable = false)
	private String invoiceNumber;

	@Column(name = "invoice_date", nullable = false)
	private Date invoiceDate;

	@ManyToOne(optional = false)
	@JoinColumn(name = "seller_id")
	private Company seller;

	@ManyToOne(optional = false)
	@JoinColumn(name = "contractor_id")
	private Company contractor;

	@ManyToOne(optional = false)
	private ChiefAccountant chiefAccountant;

	@ManyToOne(optional = false)
	private OrgHead orgHead;

	@ManyToOne(optional = false)
	private TaxRate taxRate;

	@ManyToOne(optional = false)
	private Currency currency;

	@OneToMany(mappedBy = "invoiceInfo", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<InvoiceData> invoiceDatas;

	public List<InvoiceData> getInvoiceDatas() {
		return invoiceDatas;
	}

	public void setInvoiceDatas(List<InvoiceData> invoiceDatas) {
		this.invoiceDatas = invoiceDatas;
	}

	public InvoiceInfo() {
		super();
	}

	public InvoiceInfo(Integer id, String invoiceNumber, Date invoiceDate, Company seller, Company contractor,
			ChiefAccountant chiefAccountant, OrgHead orgHead, TaxRate taxRate, Currency currency) {
		super();
		this.id = id;
		this.invoiceNumber = invoiceNumber;
		this.invoiceDate = invoiceDate;
		this.seller = seller;
		this.contractor = contractor;
		this.chiefAccountant = chiefAccountant;
		this.orgHead = orgHead;
		this.taxRate = taxRate;
		this.currency = currency;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Company getSeller() {
		return seller;
	}

	public void setSeller(Company seller) {
		this.seller = seller;
	}

	public Company getContractor() {
		return contractor;
	}

	public void setContractor(Company contractor) {
		this.contractor = contractor;
	}

	public ChiefAccountant getChiefAccountant() {
		return chiefAccountant;
	}

	public void setChiefAccountant(ChiefAccountant chiefAccountant) {
		this.chiefAccountant = chiefAccountant;
	}

	public OrgHead getOrgHead() {
		return orgHead;
	}

	public void setOrgHead(OrgHead orgHead) {
		this.orgHead = orgHead;
	}

	public TaxRate getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(TaxRate taxRate) {
		this.taxRate = taxRate;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
}
