package com.gmail.gohnxp.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Currency implements Serializable  {
	
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="curr_gen", sequenceName="curr_seq", allocationSize=1, initialValue=2)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="curr_gen")
	private Integer id;

	@Column(nullable = false)
	private Integer number;

	@Column(nullable = false)
	private String name;

	public Currency() {
		super();
	}

	public Currency(Integer id, Integer number, String name) {
		super();
		this.id = id;
		this.number = number;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFio() {
		return number;
	}

	public void setFio(Integer value) {
		this.number = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
