package com.gmail.gohnxp.domain.projection;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import com.gmail.gohnxp.domain.InvoiceInfo;

@Projection(name = "flatten", types = InvoiceInfo.class)
public interface FlattenInvoiceInfo {
	Integer getId();
	String getInvoiceNumber();
	Date getInvoiceDate();	
	@Value("#{target.seller.name}")
	String getSeller();
	@Value("#{target.contractor.name}")
	String getContractor();
	@Value("#{target.currency.name}")
	String getCurrency();
	@Value("#{target.chiefAccountant.fio}")	
	String getChiefAccountant();
	@Value("#{target.orgHead.fio}")	
	String getOrgHead();
	@Value("#{target.taxRate.value}")	
	Integer getTaxRate();
}
