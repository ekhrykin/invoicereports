package com.gmail.gohnxp.domain.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import com.gmail.gohnxp.domain.InvoiceData;

@Projection(name = "flatten", types = InvoiceData.class)
public interface FlattenInvoiceData {
	String getProductName();
	Double getProductPrice();
	Double getCount();
	@Value("#{target.measure.name}")
	String getMeasure();
}
