package com.gmail.gohnxp.domain;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class TaxRate implements Serializable  {
	
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="oh_gen", sequenceName="oh_seq", allocationSize=1, initialValue=2)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="oh_gen")
	private Integer id;

	@Column(nullable = false)
	private Integer value;

	@Column(nullable = false, name="start_date")
	private Date startDate;

	public TaxRate() {
		super();
	}

	public TaxRate(Integer id, Integer value, Date startDate) {
		super();
		this.id = id;
		this.value = value;
		this.startDate = startDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date start_date) {
		this.startDate = start_date;
	}
	
	
}
