package com.gmail.gohnxp.domain;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class ChiefAccountant implements Serializable  {
	
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ca_gen", sequenceName="ca_seq", allocationSize=1, initialValue=2)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ca_gen")
	private Integer id;

	@Column(nullable = false)
	private String fio;

	@Column(nullable = false, name="start_date")
	private Date startDate;

	public ChiefAccountant() {
		super();
	}

	public ChiefAccountant(Integer id, String fio, Date startDate) {
		super();
		this.id = id;
		this.fio = fio;
		this.startDate = startDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFio() {
		return fio;
	}

	public void setFio(String fio) {
		this.fio = fio;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date start_date) {
		this.startDate = start_date;
	}
	
	
}
