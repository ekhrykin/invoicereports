package com.gmail.gohnxp.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class InvoiceData implements Serializable  {
	
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id_gen", sequenceName="id_seq", allocationSize=1, initialValue=2)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="id_gen")
	private Integer id;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="invoice_info_id")
	private InvoiceInfo invoiceInfo;
	
	@Column(name="product_name", nullable=false)
	private String productName;
	
	@ManyToOne(optional=false)
	@JoinColumn(name="measure_id")
	private Measure measure;
	
	@Column(nullable=false)
	private Double count;

	@Column(name="product_price", nullable=false)
	private Double productPrice;
	
	public InvoiceData() {
		super();
	}

	public InvoiceData(Integer id, InvoiceInfo invoiceInfo, String productName, Measure measure, Double count,
			Double productPrice) {
		super();
		this.id = id;
		this.invoiceInfo = invoiceInfo;
		this.productName = productName;
		this.measure = measure;
		this.count = count;
		this.productPrice = productPrice;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public InvoiceInfo getInvoiceInfo() {
		return invoiceInfo;
	}

	public void setInvoiceInfo(InvoiceInfo invoiceInfo) {
		this.invoiceInfo = invoiceInfo;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Measure getMeasure() {
		return measure;
	}

	public void setMeasure(Measure measure) {
		this.measure = measure;
	}

	public Double getCount() {
		return count;
	}

	public void setCount(Double count) {
		this.count = count;
	}

	public Double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(Double productPrice) {
		this.productPrice = productPrice;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
