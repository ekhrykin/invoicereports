package com.gmail.gohnxp.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

import com.gmail.gohnxp.domain.ChiefAccountant;
import com.gmail.gohnxp.domain.Company;
import com.gmail.gohnxp.domain.Currency;
import com.gmail.gohnxp.domain.InvoiceData;
import com.gmail.gohnxp.domain.InvoiceInfo;
import com.gmail.gohnxp.domain.Measure;
import com.gmail.gohnxp.domain.OrgHead;
import com.gmail.gohnxp.domain.TaxRate;

@Configuration
public class RepositoryRestConfig extends RepositoryRestMvcConfiguration {

    @Override
    protected void configureRepositoryRestConfiguration(
            RepositoryRestConfiguration config) {
        config.exposeIdsFor(Company.class, InvoiceInfo.class, InvoiceData.class, TaxRate.class,
        		ChiefAccountant.class, Currency.class, Measure.class, OrgHead.class);
        config.setBasePath("/api");
    }
}
