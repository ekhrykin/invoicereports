package com.gmail.gohnxp.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gmail.gohnxp.domain.ReportType;
import com.gmail.gohnxp.service.ReportSearchService;


@Controller
@RequestMapping("/api/invoice_info")
public class ReportSearchController {

	private Logger logger = LoggerFactory.getLogger(ReportSearchController.class);

	@Autowired
	ReportSearchService reportSearchService;
	
	@RequestMapping(value = "{invoiceInfoId}/reports/{reportType}", 
			method = { RequestMethod.GET }, produces = "application/pdf")
	public @ResponseBody ResponseEntity<byte[]> getReport(@PathVariable("invoiceInfoId") int invoiceInfoId,
			@PathVariable("reportType") int reportType) {
		byte[] reportData = null;

		try {
			reportData = reportSearchService.search(invoiceInfoId, ReportType.values()[reportType - 1]);
		} catch (IOException ioe) {
			logger.error("Error while searching for report!", ioe);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(reportData, HttpStatus.OK);
	}
}
