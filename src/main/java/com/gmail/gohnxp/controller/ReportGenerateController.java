package com.gmail.gohnxp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gmail.gohnxp.service.ReportsGeneratorService;

@Controller
@RequestMapping("/api/invoice_info")
public class ReportGenerateController {

	private Logger logger = LoggerFactory.getLogger(ReportGenerateController.class);

	@Autowired
	ReportsGeneratorService reportsGeneratorService;

	private final static String SUCCESS_RESULT = "{result: 'success'}";
	private final static String FAIL_RESULT = "{result: 'fail'}";
	
	@RequestMapping(value = "{invoiceInfoId}/reports", produces = "application/json", 
			method = { RequestMethod.POST })
	public @ResponseBody ResponseEntity<String> generateReport(@PathVariable("invoiceInfoId") int invoiceInfoId) {

		try {
			reportsGeneratorService.generate(invoiceInfoId);
		} catch (Exception e) {
			logger.error("Can't generate reports.", e);
			return new ResponseEntity<>(FAIL_RESULT, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(SUCCESS_RESULT, HttpStatus.CREATED);
	}
}
