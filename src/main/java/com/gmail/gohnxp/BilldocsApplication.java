package com.gmail.gohnxp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BilldocsApplication {

    public static void main(String[] args) {
        SpringApplication.run(BilldocsApplication.class, args);
    }
}
