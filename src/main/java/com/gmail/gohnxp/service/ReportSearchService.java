package com.gmail.gohnxp.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.gmail.gohnxp.domain.ReportType;

@Service
public class ReportSearchService {

	@Value("${reports.first_report.dest_file_name_path_with_prefix}")
	private String firstReportPathPrefix;

	@Value("${reports.second_report.dest_file_name_path_with_prefix}")
	private String secondReportPathPrefix;

	@Value("${reports.third_report.dest_file_name_path_with_prefix}")
	private String thirdReportPathPrefix;

	public byte[] search(int invoiceInfoId, ReportType reportType) throws IOException{
		
		String reportPathPrefix = null;
		if (reportType == ReportType.REPORT1)
			reportPathPrefix = firstReportPathPrefix;
		else if (reportType == ReportType.REPORT2)
			reportPathPrefix = secondReportPathPrefix;
		else if (reportType == ReportType.REPORT3)
			reportPathPrefix = thirdReportPathPrefix;

		byte[] reportData = Files.readAllBytes(Paths.get(reportPathPrefix + invoiceInfoId + ".pdf"));
		return reportData;
	}
}
