package com.gmail.gohnxp.service;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.export.ExporterInput;
import net.sf.jasperreports.export.OutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

@SuppressWarnings("deprecation")
@Service
public class ReportsGeneratorService {

	private final static String REPORT1_TEMPLATE_NAME = "InvoiceReport1";
	private final static String REPORT2_TEMPLATE_NAME = "InvoiceReport2";
	private final static String REPORT3_TEMPLATE_NAME = "InvoiceReport3";// TODO correct

	@Value("${spring.datasource.driver-class-name}")
	private String dbDriver;

	@Value("${spring.datasource.url}")
	private String dbUrl;

	@Value("${spring.datasource.username}")
	private String dbUser;

	@Value("${spring.datasource.password}")
	private String dbPassword;

	@Value("${reports.first_report.dest_file_name_path_with_prefix}")
	private String firstReportDestPathPrefix;

	@Value("${reports.second_report.dest_file_name_path_with_prefix}")
	private String secondReportDestPathPrefix;

	@Value("${reports.third_report.dest_file_name_path_with_prefix}")
	private String thirdReportDestPathPrefix;

	public void generate(int invoiceInfoId) throws Exception {
		Connection conn = getConnection();
		generateReport(invoiceInfoId, REPORT1_TEMPLATE_NAME, firstReportDestPathPrefix, conn);
		generateReport(invoiceInfoId, REPORT2_TEMPLATE_NAME, secondReportDestPathPrefix, conn);
		generateReport(invoiceInfoId, REPORT3_TEMPLATE_NAME, thirdReportDestPathPrefix, conn);
	}

	private void generateReport(int invoiceInfoId, String reportTemplateName, String reportDestPathWithFileNamePrefix,
			Connection conn) throws Exception {
		Map<String, Object> reportParams = new HashMap<>();
		reportParams.put("INVOICE_INFO_ID", invoiceInfoId);

		JasperReport jReport = JasperCompileManager.compileReport(
				getClass().getResourceAsStream("../../../../reports/templates/" + reportTemplateName + ".jrxml"));
		JasperPrint jPrint = JasperFillManager.fillReport(jReport, reportParams, conn);

		saveReportAsPdf(jPrint, reportDestPathWithFileNamePrefix + invoiceInfoId + ".pdf");
	}

	private Connection getConnection() throws SQLException, ClassNotFoundException {
		Class.forName(dbDriver);
		Properties props = new Properties();
		props.setProperty("user", dbUser);
		props.setProperty("password", dbPassword);
		return DriverManager.getConnection(dbUrl, props);
	}

	private void saveReportAsPdf(JasperPrint jasperPrint, String resFilePath) throws Exception {
		JRPdfExporter exporter = new JRPdfExporter();

		exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
		OutputStreamExporterOutput oseo = new SimpleOutputStreamExporterOutput(resFilePath);
		exporter.setExporterOutput(oseo);
		SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
		exporter.setConfiguration(configuration);

		exporter.exportReport();
	}
}
