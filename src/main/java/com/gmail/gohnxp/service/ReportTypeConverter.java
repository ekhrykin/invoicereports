package com.gmail.gohnxp.service;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.gmail.gohnxp.domain.ReportType;

@Component
public class ReportTypeConverter implements Converter<String, ReportType> {

	@Override
	public ReportType convert(String arg) {
		return ReportType.values()[Integer.parseInt(arg)];
	}

}
